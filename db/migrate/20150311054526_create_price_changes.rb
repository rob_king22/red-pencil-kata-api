class CreatePriceChanges < ActiveRecord::Migration
  def change
    create_table :price_changes do |t|
      t.float :new_price
      t.belongs_to :item, index: true

      t.timestamps
    end
  end
end
