class AddItemIdToPriceChange < ActiveRecord::Migration
  def change
    add_column :price_changes, :item_id, :int
  end
end
