class AddPromoStartAndEndToItems < ActiveRecord::Migration
  def change
    add_column :items, :promo_start, :date
    add_column :items, :promo_end, :date
  end
end
