class PriceChange < ActiveRecord::Base
	belongs_to :item
	
	def check_for_promotion
		puts "runs after_initialize"
		item_this_belongs_to = Item.find(self.item_id)
		if(item_this_belongs_to.promo_start.nil? && item_this_belongs_to.promo_end.nil? && is_promotion_price?(item_this_belongs_to))
			start_promo(item_this_belongs_to)
		elsif(item_this_belongs_to.promo_start.nil? && DateTime.now >= item_this_belongs_to.promo_end + 30)
			start_promo(item_this_belongs_to)
		elsif(DateTime.now > item_this_belongs_to.promo_end || price_did_increase?(item_this_belongs_to) || !is_promotion_price?(item_this_belongs_to))
			end_promo(item_this_belongs_to)
		else
			puts "the promo continues"
		end
	end
	
	
	def is_promotion_price?(item)
		puts "loop gets to is_promotion_price?"
		if(self.new_price <= item.original_price * 0.95 && self.new_price >= item.original_price * 0.70)
			true
		else
			false
		end
	end
	
	def price_did_increase?(item)
		puts "loop gets to price_did_increase?"
		if(item.price_change.count > 1)
			previous_price = item.price_change.find(item.price_change.count - 2)
			puts "this is the cause of the infinite loop"
			if(self.new_price > previous_price.new_price)
				true
			else
				false
			end
		else
			false
		end
	end
	
	def start_promo(item)
		puts "create new promo"
		item.promo_start = DateTime.now
		item.promo_end = DateTime.now + 30
		item.save
	end
	
	def end_promo(item)
		puts "end current promo"
		item.promo_start = nil 
		item.promo_end = DateTime.now
		item.save
	end
end