class Item < ActiveRecord::Base
	has_many :price_change
	
	def price
		if(self.price_change.count > 0)
			if(self.price_change.count > 0)
				latest_price = self.price_change.last()
				latest_price.new_price
			else
				self.original_price
			end
		else
			self.original_price
		end
	end
	
	def redpencil_promotion?
		if(self.price_change.count > 0)
			if(self.promo_start && self.promo_end)
				if(DateTime.now < self.promo_end)
					true
				else
					false
				end
			else
				false
			end
		else
			false
		end
	end
end
