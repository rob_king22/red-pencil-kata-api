class Api::V1::ItemsController < ApplicationController
	skip_before_filter  :verify_authenticity_token
	
	def index
		items = Item.all 
		render json: items, :methods => [:price, :redpencil_promotion?]
	end
	
	def create 
		item = Item.new(item_params)
		item.save 
		render json: item 
	end
	
	def price_change
		price_change = PriceChange.new(price_change_params)
		price_change.check_for_promotion
		price_change.save
		changed_item = Item.find(price_change.item_id)
		render json: changed_item, :methods => [:price, :redpencil_promotion?]	
	end
	
	def item_params
		params.require(:item).permit(:name, :original_price)
	end
	
	def price_change_params
		params.require(:item_price_change).permit(:item_id, :new_price)
	end
end
