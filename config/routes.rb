Rails.application.routes.draw do
	root 'welcome#index'
	get 'welcome/index'
	
	namespace :api do
		namespace :v1 do 
			get '/items', to: 'items#index'
			post '/items', to: 'items#create'
			post '/items/price_change', to: 'items#price_change'
			resources :items, :price_change
		end 
	end
end
